import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue';
import Company from '../views/company.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home, 

  },
  {
    path: '/:company',
    name: 'company',
    component: Company,
    props: true,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
